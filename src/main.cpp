#include <cinttypes>
#include <cstdint>
#include <memory>
#include <optional>
#include <vector>
#include <windows.h>
#include <pdh.h>
#include <pdhmsg.h>
#include <psapi.h>
#include <strsafe.h>

void Log(const wchar_t *format, ...) {
  wchar_t linebuf[1024];
  va_list v;
  va_start(v, format);
  ::StringCbVPrintfW(linebuf, sizeof(linebuf), format, v);
  va_end(v);
  ::OutputDebugStringW(linebuf);
  wprintf(linebuf);
}

uint64_t hex_to_uint64(const wchar_t *s) {
  auto htoc = [](wchar_t c) -> int {
    return (c >= '0' && c <= '9')   ? c - '0'
           : (c >= 'a' && c <= 'f') ? c - 'a' + 10
           : (c >= 'A' && c <= 'F') ? c - 'A' + 10
                                    : -1;
  };
  uint64_t ret = 0;
  const wchar_t *p = s;
  uint32_t valid_chars = 0;
  for (; *p && valid_chars <= 16; ++p) {
    if (p == s + 1 && s[1] == 'x' && s[0] == '0') {
      valid_chars = 0;
      ret = 0;
    } else if (*p != '`') {
      int c = htoc(*p);
      if (c < 0)
        return 0;
      ret = (ret << 4) | c;
      if (ret > 0)
        ++valid_chars;
    }
  }
  return valid_chars <= 16 ? ret : 0;
}

bool getNumberArgument(wchar_t aArgName, const wchar_t *aArg, SIZE_T &aOut) {
  if (aArg[0] != L'-' && aArg[0] != L'/') {
    return false;
  }

  if (aArg[1] != aArgName) {
    return false;
  }

  uint64_t u64 = hex_to_uint64(&aArg[2]);
  ;
  aOut = static_cast<SIZE_T>(u64);
  return true;
}

void Touch(void *aAddr, SIZE_T aSize) {
  volatile uint8_t x = 0;
  auto base = reinterpret_cast<uint8_t *>(aAddr);
  for (int64_t i = 0, pages = aSize >> 12; i < pages; ++i) {
    auto pageBegin = base + (i << 12);
    int offset = rand() % (1 << 12);
    x ^= *(pageBegin + offset);
    //*reinterpret_cast<uint32_t*>(pageBegin) = rand();
  }
}

void PrintMemState() {
  MEMORYSTATUSEX statex = {sizeof(statex)};
  if (::GlobalMemoryStatusEx(&statex)) {
    Log(L"SystemMemoryUsePercentage = %d\n"
        L"TotalVirtualMemory        = %lld\n"
        L"AvailableVirtualMemory    = %lld\n"
        L"TotalPhysicalMemory       = %lld\n"
        L"AvailablePhysicalMemory   = %lld\n"
        L"TotalPageFile             = %lld\n"
        L"AvailablePageFile         = %lld\n",
        statex.dwMemoryLoad, statex.ullTotalVirtual, statex.ullAvailVirtual,
        statex.ullTotalPhys, statex.ullAvailPhys, statex.ullTotalPageFile,
        statex.ullAvailPageFile);
  } else {
    Log(L"GlobalMemoryStatusEx failed - %08lx", ::GetLastError());
  }

  PERFORMANCE_INFORMATION info;
  if (::GetPerformanceInfo(&info, sizeof(info))) {
    Log(L"TotalPageFile             = %lld\n"
        L"AvailablePageFile         = %lld\n",
        info.CommitLimit * info.PageSize,
        (info.CommitLimit - info.CommitTotal) * info.PageSize);
  } else {
    Log(L"GetPerformanceInfo failed - %08lx", ::GetLastError());
  }
}


class PerfCounter {
 protected:
  PDH_HCOUNTER mHandle;

  bool CollectInternal(PDH_RAW_COUNTER& aResult) const {
    DWORD type;
    PDH_STATUS status = ::PdhGetRawCounterValue(mHandle, &type, &aResult);
    if (status != ERROR_SUCCESS) {
      Log(L"PdhGetRawCounterValue failed - %08lx\n", status);
      return false;
    }

    if (aResult.CStatus == PDH_INVALID_DATA) {
      return false;
    }

    if (aResult.CStatus != ERROR_SUCCESS) {
      Log(L"PdhGetRawCounterValue returned invalid data - %08lx\n",
          aResult.CStatus);
      return false;
    }
    return true;
  }

 public:
  PerfCounter(PDH_HQUERY aQuery, const wchar_t* aPath) : mHandle(nullptr) {
    PDH_STATUS status = ::PdhAddCounterW(aQuery, aPath, 0, &mHandle);
    if (status != ERROR_SUCCESS) {
      Log(L"PdhAddCounterW(%s) failed - %08lx\n", aPath, status);
    }
  }

  virtual ~PerfCounter() = default;

  constexpr operator bool() const { return !!mHandle; }

  virtual std::optional<int64_t> GetLarge() {
    PDH_RAW_COUNTER raw;
    if (!CollectInternal(raw)) {
      return {};
    }
    return raw.FirstValue;
  }

  virtual std::optional<double> GetDouble() { return {}; }
};

class TwoPointsCounter : public PerfCounter {
  PDH_RAW_COUNTER mPrevious;

  bool CollectAndFormat(DWORD aFormat, PDH_FMT_COUNTERVALUE& aResult) {
    if (mPrevious.CStatus != ERROR_SUCCESS) {
      CollectInternal(mPrevious);
      return false;
    }

    PDH_RAW_COUNTER raw;
    if (!CollectInternal(raw)) {
      return false;
    }

    PDH_STATUS status = ::PdhCalculateCounterFromRawValue(
        mHandle, aFormat, &raw, &mPrevious, &aResult);
    mPrevious = raw;

    if (status != ERROR_SUCCESS) {
      Log(L"PdhCalculateCounterFromRawValue failed - %08lx\n", status);
      return false;
    }
    if (aResult.CStatus != ERROR_SUCCESS) {
      Log(L"PdhCalculateCounterFromRawValue returned invalid data - %08lx\n",
          aResult.CStatus);
      return false;
    }

    return true;
  }

 public:
  TwoPointsCounter(PDH_HQUERY aQuery, const wchar_t* aPath)
    : PerfCounter(aQuery, aPath),
      mPrevious({PDH_CSTATUS_INVALID_DATA})
  {}

  virtual std::optional<int64_t> GetLarge() override {
    PDH_FMT_COUNTERVALUE value;
    if (!CollectAndFormat(PDH_FMT_LARGE, value)) {
      return {};
    }
    return value.largeValue;
  }

  virtual std::optional<double> GetDouble() override {
    PDH_FMT_COUNTERVALUE value;
    if (!CollectAndFormat(PDH_FMT_DOUBLE, value)) {
      return {};
    }
    return value.doubleValue;
  }
};

class Perfmon {
  PDH_HQUERY mHandle;
  std::vector<std::unique_ptr<PerfCounter>> mCounters;

 public:
  Perfmon() : mHandle(nullptr) {
    PDH_STATUS status = ::PdhOpenQueryW(nullptr, 0, &mHandle);
    if (status != ERROR_SUCCESS) {
      Log(L"PdhOpenQueryW failed - %08lx\n", status);
      return;
    }

    mCounters.emplace_back(new PerfCounter(mHandle, L"\\Power Meter(_Total)\\Power"));
    mCounters.emplace_back(new TwoPointsCounter(mHandle, L"\\Processor Information(_Total)\\% Idle Time"));
    mCounters.emplace_back(new TwoPointsCounter(mHandle, L"\\Processor Information(_Total)\\% C1 Time"));
    mCounters.emplace_back(new TwoPointsCounter(mHandle, L"\\Processor Information(_Total)\\% C2 Time"));
    mCounters.emplace_back(new TwoPointsCounter(mHandle, L"\\Processor Information(_Total)\\% C3 Time"));
  }

  constexpr operator bool() const { return !!mHandle; }

  struct Result {
    int64_t mPower;
    double mIdle;
    double mC1;
    double mC2;
    double mC3;
  };

  Result Collect() {
    Result result = {};

    PDH_STATUS status = ::PdhCollectQueryData(mHandle);
    if (status != ERROR_SUCCESS) {
      Log(L"PdhCollectQueryData failed - %08lx\n", status);
      return result;
    }

    if (auto maybeResult = mCounters[0]->GetLarge()) {
      result.mPower = maybeResult.value();
    }
    if (auto maybeResult = mCounters[1]->GetDouble()) {
      result.mIdle = maybeResult.value();
    }
    if (auto maybeResult = mCounters[2]->GetDouble()) {
      result.mC1 = maybeResult.value();
    }
    if (auto maybeResult = mCounters[3]->GetDouble()) {
      result.mC2 = maybeResult.value();
    }
    if (auto maybeResult = mCounters[4]->GetDouble()) {
      result.mC3 = maybeResult.value();
    }

    return result;
  }

  ~Perfmon() {
    if (mHandle) {
      ::PdhCloseQuery(mHandle);
    }
  }
};

void PowerState() {
  Perfmon pm;
  if (!pm) {
    return;
  }

  SYSTEMTIME st;
  for (;;) {
    ::GetLocalTime(&st);;
    auto result = pm.Collect();
    Log(L"[%d-%02d-%02d %02d:%02d:%02d] %" PRId64 " %f %f %f %f\n",
        st.wYear,
        st.wMonth,
        st.wDay,
        st.wHour,
        st.wMinute,
        st.wSecond,
        result.mPower,
        result.mIdle,
        result.mC1,
        result.mC2,
        result.mC3);
    ::Sleep(30000);
  }
}

int wmain(int argc, wchar_t *argv[]) {
  if (argc <= 1) {
    // PrintMemState();
    PowerState();
    return 0;
  }

  struct PageFreer {
    void operator()(void *aAddr) { ::VirtualFree(aAddr, 0, MEM_RELEASE); }
  };
  using PageT = std::unique_ptr<void, PageFreer>;
  std::vector<PageT> pages;

  SIZE_T origMinWs, origMaxWs;
  if (!::GetProcessWorkingSetSize(::GetCurrentProcess(), &origMinWs,
                                  &origMaxWs)) {
    Log(L"GetProcessWorkingSetSize failed - %08lx\n", GetLastError());
    return 1;
  }

  Log(L"PID %d: Min WS = 0x%llx Max WS = 0x%llx\n", GetCurrentProcessId(),
      origMinWs, origMaxWs);

  for (int i = 1; i < argc; ++i) {
    SIZE_T size, minWs, maxWs;
    if (getNumberArgument(L'm', argv[i], size)) {
      if (!::SetProcessWorkingSetSize(::GetCurrentProcess(), origMinWs, size)) {
        Log(L"SetProcessWorkingSetSize failed - %08lx\n", GetLastError());
        continue;
      }

      ::GetProcessWorkingSetSize(::GetCurrentProcess(), &minWs, &maxWs);
      Log(L"[%d] Min WS = 0x%llx Max WS = 0x%llx\n", i, minWs, maxWs);
    }

    if (getNumberArgument(L'M', argv[i], size)) {
      if (!::SetProcessWorkingSetSize(::GetCurrentProcess(), -1, -1)) {
        Log(L"SetProcessWorkingSetSize failed - %08lx\n", GetLastError());
        continue;
      }
    }

    if (getNumberArgument(L'r', argv[i], size)) {
      PageT p(::VirtualAlloc(nullptr, size, MEM_RESERVE, PAGE_READWRITE));
      if (!p) {
        Log(L"VirtualAlloc(Reserve) failed - %08lx\n", GetLastError());
        continue;
      }
      Log(L"[%d] Reserved  = %p (0x%llx)\n", i, p.get(), size);
      pages.emplace_back(std::move(p));
    }

    if (getNumberArgument(L'c', argv[i], size)) {
      PageT p(::VirtualAlloc(nullptr, size, MEM_RESERVE | MEM_COMMIT,
                             PAGE_READWRITE));
      if (!p) {
        Log(L"VirtualAlloc(Commit) failed - %08lx\n", GetLastError());
        continue;
      }
      Log(L"[%d] Committed = %p (0x%llx)\n", i, p.get(), size);
      pages.emplace_back(std::move(p));
    }

    if (getNumberArgument(L'l', argv[i], size)) {
      PageT p(::VirtualAlloc(nullptr, size, MEM_RESERVE | MEM_COMMIT,
                             PAGE_READWRITE));
      if (!p) {
        Log(L"VirtualAlloc(Lock) failed - %08lx\n", GetLastError());
        continue;
      }
      Touch(p.get(), size);
      Log(L"[%d] Touched = %p (0x%llx)\n", i, p.get(), size);
      pages.emplace_back(std::move(p));
    }
  }

  getchar();
  return 0;
}
